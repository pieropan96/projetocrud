package com.pieropan.gerenciador.relatorio;

import com.pieropan.dominio.Imovel;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author matheus.matos
 */
public class GerenciadorRelatorio {

    private final HttpServletResponse response;
    private final FacesContext context;
    private final ByteArrayOutputStream arrayOutputStream;
    private final InputStream inputStream;

    public GerenciadorRelatorio() {
        context = FacesContext.getCurrentInstance();
        response = (HttpServletResponse) context.getExternalContext().getResponse();
        arrayOutputStream = new ByteArrayOutputStream();
        inputStream = getClass().getResourceAsStream("projeto-crud.jasper");
    }

    public void gerarRelatorio(List<Imovel> imoveis) {
        try {
            JasperReport report = (JasperReport) JRLoader.loadObject(inputStream);
            JasperPrint print = JasperFillManager.fillReport(
                    report, gerarHashMap(), new JRBeanCollectionDataSource(imoveis));

            JasperExportManager.exportReportToPdfStream(print, arrayOutputStream);
            response.reset();
            response.setContentType("application/pdf");
            response.setContentLength(arrayOutputStream.size());
            response.setHeader("Content-disposition", "attachment; filename=relatorio-projeto-crud.pdf");
            response.getOutputStream().write(arrayOutputStream.toByteArray());
            response.getOutputStream().flush();
            response.getOutputStream().close();
            context.responseComplete();
        } catch (JRException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private Map<String, Object> gerarHashMap() {
        Map<String, Object> parametros = new HashMap<String, Object>();
        return parametros;
    }
}