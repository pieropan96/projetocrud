package com.pieropan.gerenciador;

import com.pieropan.controller.SimulacaoController;
import com.pieropan.dominio.Simulacao;
import java.util.Calendar;
import java.util.Objects;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author matheus.matos
 */
public class GerenciadorAtualizacoes {

    public static GerenciadorAtualizacoes instancia;
    public static boolean gerenciamentoEstaSendoExecutado = false;
    private final SimulacaoController simulacaoController;

    public GerenciadorAtualizacoes() {
        simulacaoController = new SimulacaoController();
    }

    public static GerenciadorAtualizacoes getInstancia() {
        if (Objects.isNull(instancia)) {
            instancia = new GerenciadorAtualizacoes();
        }
        return instancia;
    }

    public void gerenciarSimulacoesPendentes() {
        MonitoramentoThread monitoramentoThread = new MonitoramentoThread();
        Timer timer = new Timer();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 1);
        timer.schedule(monitoramentoThread, calendar.getTime(), 60000 * 6);
    }

    public class MonitoramentoThread extends TimerTask {

        @Override
        public void run() {
            gerenciamentoEstaSendoExecutado = true;
            Simulacao simulacao = buscarSimulacao();
            while (Objects.nonNull(simulacao)) {
                atualizarSimulacao(simulacao);
                simulacao = buscarSimulacao();
            }
            gerenciamentoEstaSendoExecutado = false;
        }

        private Simulacao buscarSimulacao() {
            return simulacaoController.getSimulacoesEmAnalise();
        }

        private void atualizarSimulacao(Simulacao simulacao) {
            Object[] status = criarStatus();
            Boolean aprovado = (Boolean) status[0];
            String mensagem = (String) status[1];
            simulacao.setAprovado(aprovado);
            simulacao.setMensagem(mensagem);
            simulacaoController.save(simulacao);
        }

        private Object[] criarStatus() {
            Object[] object = new Object[2];
            object[0] = false;
            object[1] = new String();
            String mensagem;
            Random random = new Random();
            int valor = random.nextInt(4);
            if (valor == 0) {
                mensagem = "RENDA COMPROMETIDA";
            } else if (valor == 1) {
                mensagem = "NOME NEGATIVADO";
            } else if (valor == 2) {
                mensagem = "BAIXA RENDA";
            } else {
                mensagem = "APROVADO";
                object[0] = true;
            }
            object[1] = mensagem;
            return object;
        }
    }
}