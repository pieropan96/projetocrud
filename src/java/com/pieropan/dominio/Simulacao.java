package com.pieropan.dominio;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
public class Simulacao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne
    private Imovel imovel;
    @OneToOne
    private Comprador comprador;
    private boolean aprovado;
    private String mensagem;

    @Transient
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Imovel getImovel() {
        return imovel;
    }

    public void setImovel(Imovel imovel) {
        this.imovel = imovel;
    }

    public Comprador getComprador() {
        return comprador;
    }

    public void setComprador(Comprador comprador) {
        this.comprador = comprador;
    }

    public boolean isAprovado() {
        return aprovado;
    }

    public void setAprovado(boolean aprovado) {
        this.aprovado = aprovado;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getStatus() {
        if (Objects.nonNull(mensagem) && !mensagem.isEmpty()) {
            if (mensagem.equals("EM ANÁLISE")) {
                status = "EM ANÁLISE";
            } else if (aprovado) {
                status = "APROVADO";
            } else {
                status = "REPROVADO";
            }
        }
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Simulacao() {
        super();
    }

    public Simulacao(Imovel imovel, Comprador comprador, boolean aprovado, String mensagem) {
        super();
        this.imovel = imovel;
        this.comprador = comprador;
        this.aprovado = aprovado;
        this.mensagem = mensagem;
    }

}
