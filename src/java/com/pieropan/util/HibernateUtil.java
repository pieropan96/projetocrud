package com.pieropan.util;

import com.pieropan.gerenciador.GerenciadorAtualizacoes;
import java.util.Objects;
import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author matheus.matos
 */
public class HibernateUtil {

    private static HibernateUtil instancia;
    private static SessionFactory sessionFactory;

    public static HibernateUtil getInstancia() {
        if (Objects.isNull(instancia)) {
            instancia = new HibernateUtil();
        }
        return instancia;
    }

    public HibernateUtil() {
        try {
            sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
            GerenciadorAtualizacoes.getInstancia().gerenciarSimulacoesPendentes();
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public Session novaSessao() {
        Session sessaoAtual = sessionFactory.openSession();
        return sessaoAtual;
    }
}