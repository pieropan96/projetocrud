package com.pieropan.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

/**
 *
 * @author matheus.matos
 */
public class Senha {

    private static Senha instancia;

    public static Senha getInstancia() {
        if (Objects.isNull(instancia)) {
            instancia = new Senha();
        }
        return instancia;
    }

    public String senhaCriptografada(String senha) {
        MessageDigest md;
        BigInteger hash;
        String senhaMD5 = null;
        try {
            md = MessageDigest.getInstance("MD5");
            hash = new BigInteger(1, md.digest(senha.getBytes("UTF-8")));
            senhaMD5 = hash.toString(16);
        } catch (NoSuchAlgorithmException ex) {
            System.out.println(ex);
        } catch (UnsupportedEncodingException ex) {
            System.out.println(ex);
        }
        return senhaMD5;
    }
}