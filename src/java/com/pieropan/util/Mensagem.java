package com.pieropan.util;

import java.util.Objects;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author matheus.matos
 */
public class Mensagem {

    private static Mensagem instancia;
    public static final int MENSAGEM_ERRO = 0;
    public static final int MENSAGEM_SIMULACAO_SUCESSO = 1;
    public static final int MENSAGEM_OUTROS = 2;
    public static final int MENSAGEM_CPF_JA_EXISTENTE = 3;
    public static final int MENSAGEM_LOGIN_INVALIDO = 4;
    public static final int MENSAGEM_IMOVEL_SUCESSO = 5;
    public static final int MENSAGEM_LISTA_VAZIA = 6;

    public static Mensagem getInstancia() {
        if (Objects.isNull(instancia)) {
            instancia = new Mensagem();
        }
        return instancia;
    }

    public void addMessage(int mensagem) {
        FacesMessage message;
        if (mensagem == MENSAGEM_LISTA_VAZIA) {
            message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO", "Não existem imóveis para serem impresoss!");
        } else if (mensagem == MENSAGEM_IMOVEL_SUCESSO) {
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "SUCESSO", "Imóvel cadastrado com sucesso!");
        } else if (mensagem == MENSAGEM_LOGIN_INVALIDO) {
            message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO", "Login inválido!");
        } else if (mensagem == MENSAGEM_CPF_JA_EXISTENTE) {
            message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO", "CPF já cadastrado!");
        } else if (mensagem == MENSAGEM_SIMULACAO_SUCESSO) {
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "SUCESSO", "Proposta enviada com sucesso, resultado sai em alguns minutos!");
        } else if (mensagem == MENSAGEM_ERRO) {
            message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO", "Erro ao realizar simulação!");
        } else {
            message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO", "Cliente não encontrado!");
        }
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
