package com.pieropan.bean;

import com.pieropan.controller.SimulacaoController;
import com.pieropan.dominio.Comprador;
import com.pieropan.dominio.Simulacao;
import com.pieropan.util.Mensagem;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author matheus.matos
 */
@ViewScoped
@ManagedBean(name = "simulacaoBean")
public class SimulacaoBean {

    private Comprador comprador;
    private final SimulacaoController simulacaoController;
    private boolean encontrado;
    private List<Simulacao> simulacao;

    public SimulacaoBean() {
        simulacaoController = new SimulacaoController();
        encontrado = false;
        simulacao = new ArrayList();
        comprador = new Comprador();
    }

    public Comprador getComprador() {
        return comprador;
    }

    public void setComprador(Comprador comprador) {
        this.comprador = comprador;
    }

    public boolean isEncontrado() {
        return encontrado;
    }

    public void setEncontrado(boolean encontrado) {
        this.encontrado = encontrado;
    }

    public List<Simulacao> getSimulacao() {
        return simulacao;
    }

    public void setSimulacao(List<Simulacao> simulacao) {
        this.simulacao = simulacao;
    }

    public void visualizarResultado() {
        if (camposValidados()) {
            Simulacao s = simulacaoController.getSimulacao(comprador.getCpf(), comprador.getDataNascimento());
            if (Objects.isNull(s)) {
                Mensagem.getInstancia().addMessage(Mensagem.MENSAGEM_OUTROS);
            } else {
                encontrado = true;
                simulacao.add(s);
            }
        }
        limparComprador();
    }

    private void limparComprador() {
        comprador = new Comprador();
    }

    private boolean camposValidados() {
        return Objects.nonNull(comprador.getCpf()) && !comprador.getCpf().isEmpty()
                && Objects.nonNull(comprador.getDataNascimento()) && !comprador.getDataNascimento().isEmpty();
    }

}