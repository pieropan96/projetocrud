package com.pieropan.bean;

import com.pieropan.controller.ImovelController;
import com.pieropan.dominio.Imovel;
import com.pieropan.util.Mensagem;
import java.util.Objects;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author matheus.matos
 */
@ViewScoped
@ManagedBean(name = "cadastrarImovelBean")
public class CadastrarImovelBean {

    private Imovel imovel;
    public static Imovel imovelAtualizar;
    private final ImovelController imovelController;
    public boolean atualizar = false;
    private String tituloHeader;

    public CadastrarImovelBean() {
        imovel = Objects.isNull(imovelAtualizar)
                ? new Imovel() : imovelAtualizar;
        imovelController = new ImovelController();
        tituloHeader = Objects.isNull(imovelAtualizar) 
                ? "IMÓVEIS | NOVO" : "IMÓVEIS | ATUALIZAR";
    }

    public String getTituloHeader() {
        return tituloHeader;
    }

    public void setTituloHeader(String tituloHeader) {
        this.tituloHeader = tituloHeader;
    }

    public Imovel getImovel() {
        return imovel;
    }

    public void setImovel(Imovel imovel) {
        this.imovel = imovel;
    }

    public String salvar() {
        String pagina;
        if (novoObjeto()) {
            imovel.setCaminhoImagem("outros.jpg");
        }
        if (imovelController.save(imovel)) {
            Mensagem.getInstancia().addMessage(Mensagem.MENSAGEM_IMOVEL_SUCESSO);
            pagina = "listar_imovel?faces-redirect=true";
        } else {
            pagina = "cadastrar_imovel?faces-redirect=true";
        }
        limparObjetoAtualizar();
        return pagina;
    }

    public String alterar(Imovel imovel) {
        imovelAtualizar = imovel;
        return "cadastrar_imovel?faces-redirect=true";
    }

    private boolean novoObjeto() {
        return Objects.isNull(imovelAtualizar);
    }

    private void limparObjetoAtualizar() {
        imovelAtualizar = null;
    }
}
