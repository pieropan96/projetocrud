package com.pieropan.bean;

import com.pieropan.controller.LoginController;
import com.pieropan.dominio.Login;
import com.pieropan.util.Mensagem;
import com.pieropan.util.Senha;
import java.util.Objects;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author matheus.matos
 */
@SessionScoped
@ManagedBean(name = "loginBean")
public class LoginBean {

    private Login login;
    private final LoginController loginController;
    private boolean logado = false;

    public LoginBean() {
        login = new Login();
        loginController = new LoginController();
    }

    public Login getLogin() {
        return login;
    }

    public boolean isLogado() {
        return logado;
    }

    public void setLogado(boolean logado) {
        this.logado = logado;
    }

    public String logar() {
        if (camposValidados()) {
            login.setSenha(Senha.getInstancia().senhaCriptografada(login.getSenha()));
            boolean logou = loginController.logar(login);
            if (logou) {
                logado = true;
                login = new Login();
                return "index";
            }
        }
        Mensagem.getInstancia().addMessage(Mensagem.MENSAGEM_LOGIN_INVALIDO);
        return "";
    }

    private boolean camposValidados() {
        return Objects.nonNull(login.getNome()) && !login.getNome().isEmpty()
                && Objects.nonNull(login.getSenha()) && !login.getSenha().isEmpty();
    }
}
