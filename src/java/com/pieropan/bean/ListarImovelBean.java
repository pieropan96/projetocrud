package com.pieropan.bean;

import com.pieropan.controller.ImovelController;
import com.pieropan.controller.SimulacaoController;
import com.pieropan.dominio.Imovel;
import com.pieropan.dominio.Simulacao;
import com.pieropan.gerenciador.relatorio.GerenciadorRelatorio;
import com.pieropan.util.Mensagem;
import java.util.List;
import java.util.Objects;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author matheus.matos
 */
@ViewScoped
@ManagedBean(name = "listarImovelBean")
public class ListarImovelBean {

    private List<Imovel> imoveis;

    public ListarImovelBean() {
        imoveis = new ImovelController().findAll();
    }

    public List<Imovel> getImoveis() {
        return imoveis;
    }

    public void setImoveis(List<Imovel> imoveis) {
        this.imoveis = imoveis;
    }

    public void excluir(Imovel imovel) {
        SimulacaoController simulacaoController = new SimulacaoController();
        List<Simulacao> simulacoesPorImovel = simulacaoController.getSimulacoesPorImovel(imovel);
        if (Objects.nonNull(simulacoesPorImovel) && !simulacoesPorImovel.isEmpty()) {
            simulacaoController.excluir(simulacoesPorImovel);
        }
        ImovelController imovelController = new ImovelController();
        imovelController.excluir(imovel);
        imoveis = imovelController.findAll();
    }

    public void gerarRelatorio() {
        List<Imovel> imoveis = new ImovelController().findAll();
        if (Objects.nonNull(imoveis) && !imoveis.isEmpty()) {
            new GerenciadorRelatorio().gerarRelatorio(new ImovelController().findAll());
            return;
        }
        Mensagem.getInstancia().addMessage(Mensagem.MENSAGEM_LISTA_VAZIA);
    }   
}