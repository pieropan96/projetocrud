package com.pieropan.bean;

import com.pieropan.controller.CompradorController;
import com.pieropan.controller.ImovelController;
import com.pieropan.controller.LoginController;
import com.pieropan.controller.SimulacaoController;
import com.pieropan.dao.ImovelDAO;
import com.pieropan.dominio.Comprador;
import com.pieropan.dominio.Imovel;
import com.pieropan.dominio.Login;
import com.pieropan.dominio.Simulacao;
import com.pieropan.util.Mensagem;
import com.pieropan.util.Senha;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author matheus.matos
 */
@ViewScoped
@ManagedBean(name = "indexBean")
public class IndexBean {

    private boolean logado = false;
    private Comprador comprador;
    private List<Imovel> imoveis;
    private final SimulacaoController simulacaoController;
    private final CompradorController compradorController;
    private final LoginController loginController;

    public IndexBean() {
        comprador = new Comprador();
        simulacaoController = new SimulacaoController();
        compradorController = new CompradorController();
        loginController = new LoginController();
        imoveis = new ImovelController().findAll();
        if (imoveis.isEmpty() && !loginController.jaPossuiLogin()) {
            popularBancoDeDados();
            criarAdministrador();
        }
    }

    public boolean isLogado() {
        return logado;
    }

    public void setLogado(boolean logado) {
        this.logado = logado;
    }

    public Comprador getComprador() {
        return comprador;
    }

    public void setComprador(Comprador comprador) {
        this.comprador = comprador;
    }

    public List<Imovel> getImoveis() {
        return imoveis;
    }

    public void setImoveis(List<Imovel> imoveis) {
        this.imoveis = imoveis;
    }

    private List<Imovel> popularBancoDeDados() {
        imoveis.add(new Imovel("Reserva Bandeirantes", "Cada momento é especial. Então, nada mais justo que um lugar especial para você apreciá-lo com qualidade, conforto, modernidade e bem-estar.\n"
                + "\n"
                + "O Reserva Bandeirantes foi pensado e desenvolvido com muito cuidado e atenção para que todos os dias se tornem especiais. "
                + "Possui área de lazer completa com piscina, churrasqueira, pet place, playground, "
                + "academia e muito mais para que a diversão faça parte do seu dia-a-dia. Além disso, a "
                + "tecnologia está presente para a sua segurança.\n"
                + "\n"
                + "Visite o decorado e fale com um corretor!",
                "Bandeirante - Juiz de Fora", new BigDecimal("190.0000"), "bandeirante.jpg"));
        imoveis.add(new Imovel("Park Quinet", "A Inter Construtora idealiza a sua qualidade de vida quando o assunto é casa própria. "
                + "Nossa preocupação é construir um imóvel sustentável e perto de todos os serviços que são essências à você e a sua família. "
                + "Pensando nisso, escolhemos o bairro Santa Terezinha para acolher o Park Quinet, "
                + "o imóvel perfeito para quem quer estar próximo ao centro, com diversas opções de transporte, lazer e comércio. "
                + "Está na hora de você viver com muito mais qualidade de vida no seu novo apartamento.",
                "Santa Terezinha - Juiz de Fora", new BigDecimal("210.0000"), "terezinha.jpg"));
        imoveis.add(new Imovel("Unique Rio Preto", "O empreendimento é completo e de grande valorização, uma vez que fica próximo à outros condomínios de alto padrão. Localizado próximo ao Carrefour, tem em seu projeto os apartamentos inteligentes. São referência no mercado e reúne em um só lugar, características que o tornam econômico, confortável e sustentável.\n"
                + "\n"
                + "Ele é ideal para ser feliz e para morar bem. Possui um excelente custo benefício e é, sem dúvida, um dos melhores investimentos a se fazer. Afinal, investir em imóveis é sinônimo de segurança financeira a longo prazo.","São José do Rio Preto",new BigDecimal("310.0000"),"rio_preto.jpg"));
        ImovelDAO.getInstancia().save(imoveis);
        return imoveis;
    }

    public void realizarSimulacao(Imovel imovel) {
        boolean sucesso = false;
        boolean compradorJaCadastrado = compradorController.compradorJaCadastrado(comprador.getCpf());
        if (compradorJaCadastrado) {
            Mensagem.getInstancia().addMessage(Mensagem.MENSAGEM_CPF_JA_EXISTENTE);
            return;
        }
        boolean compradorPersistido = compradorController.save(comprador);
        if (compradorPersistido) {
            sucesso = simulacaoController.save(new Simulacao(imovel, comprador, false, "EM ANÁLISE"));
        }
        Mensagem.getInstancia().addMessage(sucesso ? Mensagem.MENSAGEM_SIMULACAO_SUCESSO : Mensagem.MENSAGEM_ERRO);
        limparComprador();
    }

    private void limparComprador() {
        comprador = new Comprador();
    }

    private void criarAdministrador() {
        String senha = "projetocrud";
        String senhaMD5 = Senha.getInstancia().senhaCriptografada(senha);
        loginController.criarAdministrador(
                new Login("admin", Objects.isNull(senhaMD5) ? senha : senhaMD5));
    }

}
