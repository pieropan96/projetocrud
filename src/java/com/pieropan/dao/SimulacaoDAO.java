package com.pieropan.dao;

import com.pieropan.dominio.Imovel;
import com.pieropan.dominio.Simulacao;
import com.pieropan.gerenciador.GerenciadorAtualizacoes;
import com.pieropan.util.HibernateUtil;
import java.util.List;
import java.util.Objects;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author matheus.matos
 */
public class SimulacaoDAO {

    private static SimulacaoDAO instancia;
    private static Session sessao;
    private static Transaction transaction;
    private final String SITUACAO_EM_ANALISE = "SELECT * FROM simulacao s WHERE s.mensagem = "
            + "'EM ANÁLISE' ORDER BY s.id ASC LIMIT 1";
    private final String GET_SIMULACAO = "SELECT s FROM Simulacao s INNER JOIN s.comprador c "
            + "WHERE c.cpf = :cpf AND c.dataNascimento = :dataNascimento";
    private final String GET_SIMULACAO_POR_IMOVEL = "SELECT s FROM Simulacao s INNER JOIN s.imovel i "
            + "WHERE i.id = :id";

    public static SimulacaoDAO getInstancia() {
        if (Objects.isNull(instancia)) {
            instancia = new SimulacaoDAO();
        }
        return instancia;
    }

    private void novaSessao(boolean abrirTransacao) {
        sessao = HibernateUtil.getInstancia().novaSessao();
        if (abrirTransacao) {
            transaction = sessao.beginTransaction();
        }
    }

    public boolean save(Simulacao simulacao) {
        novaSessao(true);
        boolean save = false;
        try {
            sessao.saveOrUpdate(simulacao);
            transaction.commit();
            save = true;
        } catch (HibernateException ex) {
            transaction.rollback();
            System.out.println(ex);
        } finally {
            if (sessao.isOpen()) {
                sessao.flush();
                sessao.close();
            }
        }
        return save;
    }

    public Simulacao getSimulacao(String cpf, String dataNascimento) {
        novaSessao(false);
        Simulacao simulacao;
        try {
            Query query = sessao.createQuery(GET_SIMULACAO);
            query.setString("cpf", cpf);
            query.setString("dataNascimento", dataNascimento);
            simulacao = (Simulacao) query.uniqueResult();
        } catch (HibernateException ex) {
            System.out.println(ex);
            return null;
        } finally {
            if (sessao.isOpen()) {
                sessao.flush();
                sessao.close();
            }
        }
        return simulacao;
    }

    public Simulacao getSimulacoesEmAnalise() {
        novaSessao(false);
        Simulacao simulacao;
        try {
            SQLQuery query = sessao.createSQLQuery(SITUACAO_EM_ANALISE);
            query.addEntity(Simulacao.class.getName());
            simulacao = (Simulacao) query.uniqueResult();
        } catch (HibernateException ex) {
            System.out.println(ex);
            return null;
        } finally {
            if (sessao.isOpen()) {
                sessao.flush();
                sessao.close();
            }
        }
        return simulacao;
    }

    public List<Simulacao> getSimulacoesPorImovel(Imovel imovel) {
        novaSessao(false);
        List<Simulacao> simulacoes = null;
        try {
            Query query = sessao.createQuery(GET_SIMULACAO_POR_IMOVEL);
            query.setInteger("id", imovel.getId());
            simulacoes = (List<Simulacao>) query.list();
        } catch (HibernateException ex) {
            System.out.println(ex);
        } finally {
            if (sessao.isOpen()) {
                sessao.flush();
                sessao.close();
            }
        }
        return simulacoes;
    }

    public void excluir(List<Simulacao> simulacoes) {
        if (GerenciadorAtualizacoes.gerenciamentoEstaSendoExecutado) {
            System.out.println("Tratativa para que não haja conflito de dados...");
            return;
        }

        try {
            for (Simulacao s : simulacoes) {
                novaSessao(true);
                sessao.delete(s);
                transaction.commit();
            }
        } catch (HibernateException ex) {
            transaction.rollback();
            System.out.println(ex);
        } finally {
            if (sessao.isOpen()) {
                sessao.flush();
                sessao.close();
            }
        }
    }
}
