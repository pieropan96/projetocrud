package com.pieropan.dao;

import com.pieropan.dominio.Imovel;
import com.pieropan.util.HibernateUtil;
import java.util.List;
import java.util.Objects;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author matheus.matos
 */
public class ImovelDAO {

    private static ImovelDAO instancia;
    private static Session sessao;
    private static Transaction transaction;
    private final String FIND_ALL = "SELECT i FROM Imovel i";

    public static ImovelDAO getInstancia() {
        if (Objects.isNull(instancia)) {
            instancia = new ImovelDAO();
        }
        return instancia;
    }

    private void novaSessao(boolean abrirTransacao) {
        sessao = HibernateUtil.getInstancia().novaSessao();
        if (abrirTransacao) {
            transaction = sessao.beginTransaction();
        }
    }

    public List<Imovel> findAll() {
        novaSessao(false);
        List<Imovel> imoveis = null;
        try {
            Query query = sessao.createQuery(FIND_ALL);
            imoveis = (List<Imovel>) query.list();
        } catch (HibernateException ex) {
            System.out.println(ex);
        } finally {
            if (sessao.isOpen()) {
                sessao.flush();
                sessao.close();
            }
        }
        return imoveis;
    }

    public void save(List<Imovel> imoveis) {
        try {
            for (Imovel imovel : imoveis) {
                novaSessao(true);
                sessao.save(imovel);
                transaction.commit();
            }
        } catch (HibernateException ex) {
            transaction.rollback();
            System.out.println(ex);
        } finally {
            if (sessao.isOpen()) {
                sessao.flush();
                sessao.close();
            }
        }
    }

    public void excluir(Imovel imovel) {
        novaSessao(true);
        try {
            sessao.delete(imovel);
            transaction.commit();
        } catch (HibernateException ex) {
            System.out.println(ex);
        } finally {
            if (sessao.isOpen()) {
                sessao.flush();
                sessao.close();
            }
        }
    }

    public boolean save(Imovel imovel) {
        novaSessao(true);
        boolean save = false;
        try {
            sessao.saveOrUpdate(imovel);
            transaction.commit();
            save = true;
        } catch (HibernateException ex) {
            System.out.println(ex);
        } finally {
            if (sessao.isOpen()) {
                sessao.flush();
                sessao.close();
            }
        }
        return save;
    }
}