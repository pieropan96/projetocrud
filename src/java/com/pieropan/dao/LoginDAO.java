package com.pieropan.dao;

import com.pieropan.dominio.Login;
import com.pieropan.util.HibernateUtil;
import java.util.Objects;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author matheus.matos
 */
public class LoginDAO {

    private static LoginDAO instancia;
    private static Session sessao;
    private static Transaction transaction;
    private final String GET_LOGIN = "SELECT l FROM Login l WHERE nome = :nome AND senha = :senha";

    public static LoginDAO getInstancia() {
        if (Objects.isNull(instancia)) {
            instancia = new LoginDAO();
        }
        return instancia;
    }

    private void novaSessao(boolean abrirTransacao) {
        sessao = HibernateUtil.getInstancia().novaSessao();
        if (abrirTransacao) {
            transaction = sessao.beginTransaction();
        }
    }

    public void criarAdministrador(Login login) {
        novaSessao(true);
        try {
            sessao.save(login);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.out.println(ex);
        } finally {
            if (sessao.isOpen()) {
                sessao.flush();
                sessao.close();
            }
        }
    }

    public boolean logar(Login login) {
        novaSessao(false);
        boolean logar = false;
        try {
            Query query = sessao.createQuery(GET_LOGIN);
            query.setString("nome", login.getNome());
            query.setString("senha", login.getSenha());
            Login l = (Login) query.uniqueResult();
            logar = Objects.nonNull(l);
        } catch (HibernateException ex) {
            System.out.println(ex);
        } finally {
            if (sessao.isOpen()) {
                sessao.flush();
                sessao.close();
            }
        }
        return logar;
    }

    public boolean jaPossuiLogin() {
        novaSessao(false);
        boolean jaPossui = false;
        try {
            Query query = sessao.createQuery("SELECT l FROM Login l");
            Login login = (Login) query.uniqueResult();
            jaPossui = Objects.nonNull(login);
        } catch (HibernateException ex) {
            System.out.println(ex);
        } finally {
            if (sessao.isOpen()) {
                sessao.flush();
                sessao.close();
            }
        }
        return jaPossui;
    }
}