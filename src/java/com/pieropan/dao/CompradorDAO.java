package com.pieropan.dao;

import com.pieropan.dominio.Comprador;
import com.pieropan.util.HibernateUtil;
import java.util.Objects;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author matheus.matos
 */
public class CompradorDAO {

    private static CompradorDAO instancia;
    private static Session sessao;
    private static Transaction transaction;
    private final String GET_CPF = "SELECT c FROM Comprador c WHERE c.cpf = :cpf";

    public static CompradorDAO getInstancia() {
        if (Objects.isNull(instancia)) {
            instancia = new CompradorDAO();
        }
        return instancia;
    }

    private void novaSessao(boolean abrirTransacao) {
        sessao = HibernateUtil.getInstancia().novaSessao();
        if (abrirTransacao) {
            transaction = sessao.beginTransaction();
        }
    }

    public boolean save(Comprador comprador) {
        novaSessao(true);
        boolean save = false;
        try {
            sessao.save(comprador);
            transaction.commit();
            save = true;
        } catch (HibernateException ex) {
            transaction.rollback();
            System.out.println(ex);
        } finally {
            if (sessao.isOpen()) {
                sessao.flush();
                sessao.close();
            }
        }
        return save;
    }

    public boolean compradorJaCadastrado(String cpf) {
        novaSessao(false);
        boolean compradorJaCadastrado = false;
        try {
            Query query = sessao.createQuery(GET_CPF);
            query.setString("cpf", cpf);
            Comprador comprador = (Comprador) query.uniqueResult();
            compradorJaCadastrado = Objects.nonNull(comprador);
        } catch (HibernateException ex) {
            System.out.println(ex);
        } finally {
            if (sessao.isOpen()) {
                sessao.flush();
                sessao.close();
            }
        }
        return compradorJaCadastrado;
    }
}