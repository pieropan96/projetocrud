package com.pieropan.conexao;

import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceException;

/**
 *
 * @author matheus.matos
 */
public class Conexao {

    private static EntityManager em;
    private EntityManagerFactory emf;
    private static Conexao instancia;

    public static Conexao getInstancia() {
        if (Objects.isNull(instancia)) {
            instancia = new Conexao();
        }
        return instancia;
    }

    public Conexao() {
        try {
            //emf = Persistence.createEntityManagerFactory("ProjetoCrudPU");
            //em = emf.createEntityManager();
        } catch (PersistenceException ex) {
            System.out.println(ex);
        }
    }

    public EntityManager getEntityManager() {
        if (Objects.isNull(instancia)) {
            instancia = new Conexao();
        }
        return em;
    }
}