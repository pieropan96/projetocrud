package com.pieropan.controller;

import com.pieropan.dao.ImovelDAO;
import com.pieropan.dominio.Imovel;
import java.util.List;

/**
 *
 * @author matheus.matos
 */
public class ImovelController {

    public boolean save(Imovel imovel){
        return ImovelDAO.getInstancia().save(imovel);
    }
    public List<Imovel> findAll() {
        return ImovelDAO.getInstancia().findAll();
    }
    
    public void excluir(Imovel imovel){
        ImovelDAO.getInstancia().excluir(imovel);
    }
}