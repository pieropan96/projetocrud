package com.pieropan.controller;

import com.pieropan.dao.LoginDAO;
import com.pieropan.dominio.Login;

/**
 *
 * @author matheus.matos
 */
public class LoginController {

    public void criarAdministrador(Login login) {
        LoginDAO.getInstancia().criarAdministrador(login);
    }

    public boolean logar(Login login) {
        return LoginDAO.getInstancia().logar(login);
    }
    
    public boolean jaPossuiLogin(){
        return LoginDAO.getInstancia().jaPossuiLogin();
    }
}