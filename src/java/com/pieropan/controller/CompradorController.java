package com.pieropan.controller;

import com.pieropan.dao.CompradorDAO;
import com.pieropan.dominio.Comprador;

/**
 *
 * @author matheus.matos
 */
public class CompradorController {

    public boolean save(Comprador comprador) {
        return CompradorDAO.getInstancia().save(comprador);
    }
    
    public boolean compradorJaCadastrado(String cpf){
        return CompradorDAO.getInstancia().compradorJaCadastrado(cpf);
    }
}