package com.pieropan.controller;

import com.pieropan.dao.SimulacaoDAO;
import com.pieropan.dominio.Imovel;
import com.pieropan.dominio.Simulacao;
import java.util.List;

/**
 *
 * @author matheus.matos
 */
public class SimulacaoController {

    public boolean save(Simulacao simulacao) {
        return SimulacaoDAO.getInstancia().save(simulacao);
    }

    public Simulacao getSimulacao(String cpf, String dataNascimento) {
        return SimulacaoDAO.getInstancia().getSimulacao(cpf, dataNascimento);
    }

    public Simulacao getSimulacoesEmAnalise() {
        return SimulacaoDAO.getInstancia().getSimulacoesEmAnalise();
    }

    public List<Simulacao> getSimulacoesPorImovel(Imovel imovel) {
        return SimulacaoDAO.getInstancia().getSimulacoesPorImovel(imovel);
    }

    public void excluir(List<Simulacao> simulacoes) {
        SimulacaoDAO.getInstancia().excluir(simulacoes);
    }
}
